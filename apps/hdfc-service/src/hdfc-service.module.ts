import { Module } from '@nestjs/common';
import { HdfcServiceController } from './hdfc-service.controller';
import { HdfcServiceService } from './hdfc-service.service';

@Module({
  imports: [],
  controllers: [HdfcServiceController],
  providers: [HdfcServiceService],
})
export class HdfcServiceModule {}
