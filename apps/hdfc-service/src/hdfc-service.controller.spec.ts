import { Test, TestingModule } from '@nestjs/testing';
import { HdfcServiceController } from './hdfc-service.controller';
import { HdfcServiceService } from './hdfc-service.service';

describe('HdfcServiceController', () => {
  let hdfcServiceController: HdfcServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [HdfcServiceController],
      providers: [HdfcServiceService],
    }).compile();

    hdfcServiceController = app.get<HdfcServiceController>(HdfcServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(hdfcServiceController.getHello()).toBe('Hello World!');
    });
  });
});
