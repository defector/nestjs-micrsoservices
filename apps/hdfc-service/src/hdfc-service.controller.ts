import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { CheckAccountRequest } from 'apps/icici-service/src/check-account-request.dto';
import { AddUserRequest } from './add-user-request.dto';
import { HdfcServiceService } from './hdfc-service.service';

@Controller()
export class HdfcServiceController {
  constructor(private readonly hdfcServiceService: HdfcServiceService) {}

  @Get()
  getHello(): string {
    return this.hdfcServiceService.getHello();
  }

  @MessagePattern('handshake')
  async handshake(){
    console.log('Recivied handshake from parent')
    return "HDFC- Handsake establised";
  }

  @MessagePattern('ADD_USER')
  async addUser(user: AddUserRequest){
    return this.hdfcServiceService.addUser(user);
  }

  @MessagePattern('CHECK_ACCOUNT')
  async checkAccount(checkAccountRequest: CheckAccountRequest){
    return this.hdfcServiceService.checkAccount(checkAccountRequest.mobileNumber);
  }
}
