import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AddUserRequest } from './add-user-request.dto';
import { CheckAccountRequest } from './check-account-request.dto';
import { IciciServiceService } from './icici-service.service';
import { NotifiyData } from './notify-data.dto';

@Controller()
export class IciciServiceController {
  constructor(private readonly iciciServiceService: IciciServiceService) {}

  @Get()
  getHello(): string {
    return this.iciciServiceService.getHello();
  }

  @MessagePattern('handshake')
  async handshake(){
    console.log('Recivied handshake from parent')
    return "ICICI- Handsake establised";
  }

  @MessagePattern('ADD_USER')
  async addUser(user: AddUserRequest){
    return this.iciciServiceService.addUser(user);
  }

  @MessagePattern('CHECK_ACCOUNT')
  async checkAccount(checkAccountRequest: CheckAccountRequest){
    return this.iciciServiceService.checkAccount(checkAccountRequest.mobileNumber);
  }
}
