import { Injectable } from '@nestjs/common';
import { AddUserRequest } from './add-user-request.dto';
import { v4 as uuidv4 } from 'uuid';


@Injectable()
export class IciciServiceService {
  private readonly users = [];

  getHello(): string {
    return 'Hello World!';
  }

  addUser(user: AddUserRequest) {
   const userId = uuidv4();
   const userResponse = {
      userId,
      name: user.name,
      mobileNumber: user.mobileNumber
   }
   this.users.push(userResponse);
   return userResponse
  }

  checkAccount(mobileNumber: string) {
    for(const user of this.users){
      if(user.mobileNumber === mobileNumber){
        return true
      }
    }
    return false;
  }
}
