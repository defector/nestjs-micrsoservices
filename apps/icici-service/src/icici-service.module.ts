import { Module } from '@nestjs/common';
import { IciciServiceController } from './icici-service.controller';
import { IciciServiceService } from './icici-service.service';

@Module({
  imports: [],
  controllers: [IciciServiceController],
  providers: [IciciServiceService],
})
export class IciciServiceModule {}
