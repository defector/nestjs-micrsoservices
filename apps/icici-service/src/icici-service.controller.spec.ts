import { Test, TestingModule } from '@nestjs/testing';
import { IciciServiceController } from './icici-service.controller';
import { IciciServiceService } from './icici-service.service';

describe('IciciServiceController', () => {
  let iciciServiceController: IciciServiceController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [IciciServiceController],
      providers: [IciciServiceService],
    }).compile();

    iciciServiceController = app.get<IciciServiceController>(IciciServiceController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(iciciServiceController.getHello()).toBe('Hello World!');
    });
  });
});
