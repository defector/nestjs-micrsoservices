import { Body, Controller, Get, Post } from '@nestjs/common';
import { AddUserToTenantRequest } from './add-user-request.dto';
import { AppService } from './app.service';
import { CheckAccountRequest } from './check-account-request.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async handshake(): Promise<any> {
    return await this.appService.handshake();
  }

  @Post("/add-user")
  async addUserToTenant(@Body() adduserRequest: AddUserToTenantRequest){
    return await this.appService.addUserToTenant(adduserRequest);
  }

  @Post("/check-account")
  async checkAccount(@Body() checkAccountRequest: CheckAccountRequest) {
    return await this.appService.checkAccount(checkAccountRequest);
  }
}
