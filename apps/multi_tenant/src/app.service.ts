import { Injectable } from '@nestjs/common';
import { AddUserToTenantRequest } from './add-user-request.dto';
import { CheckAccountRequest } from './check-account-request.dto';
import {TenantMediatorService} from './tenant-mediator.service';
@Injectable()
export class AppService {
  
  constructor(private readonly tenantMediatorService: TenantMediatorService) {}
  
  async handshake(): Promise<any> {
    const iciciHandshake = await this.tenantMediatorService.sendRequest("ICICI", "handshake", {})
    const hdfcHandshake = await this.tenantMediatorService.sendRequest("HDFC", "handshake", {})
    console.log({iciciHandshake, hdfcHandshake});
    return {iciciHandshake, hdfcHandshake};
  }

  async addUserToTenant(adduserRequest: AddUserToTenantRequest){
    return await this.tenantMediatorService.sendRequest(adduserRequest.lender,"ADD_USER", {user: {name: adduserRequest.name, mobileNumber: adduserRequest.mobileNumber}});
  }

  async checkAccount(checkAccountRequest: CheckAccountRequest) {
    return await this.tenantMediatorService.sendRequest(checkAccountRequest.lender, "CHECK_ACCOUNT", {mobileNumber: checkAccountRequest.mobileNumber});
  }
}
