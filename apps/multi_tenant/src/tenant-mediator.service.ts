import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { timeout } from 'rxjs';


enum Lender {
    ICICI = 'ICICI',
    HDFC = 'HDFC'
}

@Injectable()
export class TenantMediatorService {
 constructor(
     @Inject('ICICI_SERVICE') private iciciServiceClient: ClientProxy,
     @Inject('HDFC_SERVICE') private hdfcServiceClient: ClientProxy
    ){}

    async sendRequest(lender?: string, action?: string, paylod?: any): Promise<any> {
      const lenderService = this.getLenderService(lender);
      return await lenderService.send(action, paylod).pipe(timeout(5000)).toPromise();
    }

    private getLenderService(lenderName: string){
      switch(lenderName){
        case Lender.HDFC:
            return this.hdfcServiceClient;
        case Lender.ICICI:
            return this.iciciServiceClient;
        default:
            throw new Error('invalid Lender')
    }
  }
}
