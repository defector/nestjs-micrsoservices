import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TenantMediatorService} from './tenant-mediator.service';

@Module({
  imports: [ClientsModule.register([
    {
      name:'ICICI_SERVICE',
      transport: Transport.TCP,
      options:{
        port:3002
      }
    },
    {
      name:'HDFC_SERVICE',
      transport: Transport.TCP,
      options:{
        port:3001
      }
    }
  ])],
  controllers: [AppController],
  providers: [AppService, TenantMediatorService],
})
export class AppModule {}
