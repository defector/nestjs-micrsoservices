export interface AddUserToTenantRequest{
    name: string,
    mobileNumber: string,
    lender: string
}