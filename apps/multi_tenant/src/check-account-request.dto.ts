export interface CheckAccountRequest{
    lender: string;
    mobileNumber: string;
}