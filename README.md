# Nestjs Micrsoservices

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## To run the project do the following step


```


npm install
npm -g nest@cli
npm run start:dev # this will start main service (mutlti tenant)
npm run start:dev hdfc-service # this will start hdfc service
npm run start:dev icici-service # this will start icici service
```
